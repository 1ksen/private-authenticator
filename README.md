# TOTP Web Application (Apache + PHP + JS)

## Usage
1. clone this project on your DocumentRoot
2. Install and enable PHP
3. Set your Secret to httpd

```
echo OTPSEC=XXXXXXXXXXXXXXXX >> /etc/sysconfig/httpd
```

4. Set httpd configuration

```
echo "PassEnv OTPSEC" > /etc/httpd/conf.d/otpsec.conf
```

5. Restart Apache

## Reference
https://github.com/qoomon/otp-authenticator-webapp

## Changes from the reference document

* Changed to have SECRET on the configuration file.

## Image

![](./main.png)
